$(document).ready(function() {
    fixBold();
	initCheckboxes();
	
	$('#top-line .fleft a, #map-block .btn-1').click(function(e) {
		e.preventDefault();
		$('#modal').css('display', 'block');
		$('#modal-call').css('display', 'block');
	});
	
	$('#top-line .fright a').click(function(e) {
		e.preventDefault();
		$('#modal').css('display', 'block');
		$('#modal-mail').css('display', 'block');
	});
	
	$('#modal .shadow').click(function() {
		$('#modal').css('display', 'none');
		$('#modal .m-content>div').css('display', 'none');
	});
});

var fixBold = function() {
    $('#top-menu ul a').hover(function() {
        $(this).css('font-family', 'Roboto Light');
        var w = $(this).width();
        $(this).removeAttr('style');
        $(this).css('width', w+'px');
    });
}

var initCheckboxes = function() {
	$('.c-checkbox').each(function(i, el) {
		var checkbox = $('input', $(el));
		if(checkbox.attr('checked') != undefined) {
			$(el).addClass('checked');
		}
	});

	$('.c-checkbox').click(function() {
		var checkbox = $('input', $(this));
		if(checkbox.attr('checked') != undefined) {
			checkbox.removeAttr('checked');
			$(this).removeClass('checked');
		} else {
			checkbox.attr('checked', 'checked');
			$(this).addClass('checked');
		}
		
		checkbox.change();
	});
}